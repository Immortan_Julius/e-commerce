import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';


@Component({
  selector: 'app-shopping-cart nz-demo-table-basic',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
  
})
export class ShoppingCartComponent implements OnInit {

  productsList : any[] = [];
  isThereAnyItemToShow = false;

  constructor(private route: Router, private apiService: ApiService) { }
  

  ngOnInit(): void {
    this.productsList = this.apiService.myCart;

    this.isThereAnyItemToShow = this.productsList.length > 0;
  }

  backToStore(){
    this.route.navigateByUrl('home');
  }

}
