import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { ProductsComponent } from './components/products/products.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ProductsComponent },
  { path: 'cart', component: ShoppingCartComponent },
];

@NgModule({
  imports: [
    [RouterModule.forRoot(routes)],
  ],
  exports: [
    [RouterModule]
  ]
})
export class AppRoutingModule { }