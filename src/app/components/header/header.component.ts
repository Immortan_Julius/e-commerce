import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  notificaciones: number = 0;

  constructor(private route: Router, private apiService: ApiService) { }

  ngOnInit(): void {
    console.log('ESTO ES UNA IMPRESION DE CONSOLA');
    this.notificaciones = this.apiService.myCart.length;
  }

  onComprasClick(){
    this.route.navigateByUrl('cart');
  }

  onUsuarioClick(){
    alert('Hiciste click en usuario');
  }

  onHomeAgain(){
    this.route.navigateByUrl('home');
  }
}
